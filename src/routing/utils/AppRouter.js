import HomePage from "../pages/HomePage";
import LoginPage from "../pages/LoginPage";
import SignUpPage from "../pages/SignUpPage";
import AboutMePage from "../pages/AboutMePage";
import AppNavbarWithBootstrap from "../navbar/AppNavbarWithBootstrap";
import {Route, Routes} from "react-router-dom";

export default function AppRouter() {

    let userToPass ={
        name: "ada",
        age: 13
    }

    let teacherrToPass ={
        name: "ada teacher",
        age: 13+30
    }
    return (

        <Routes>
            <Route path="/" element={<HomePage/>}/>
            <Route path="/login" element={<LoginPage/>}/>
            <Route path="/signup" element={<SignUpPage/>}/>
            <Route path="/about" element={<AboutMePage user={userToPass} teacher={teacherrToPass}/>}/>
        </Routes>

    )
}
