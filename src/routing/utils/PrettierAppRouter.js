import {Navigate, useRoutes} from 'react-router-dom';
import LoginPage from "../pages/LoginPage";
import SimpleLayout from "../../layout/SimpleLayout";
import Page404 from "../pages/Page404";
import AboutMePage from "../pages/AboutMePage";
import HomePage from "../pages/HomePage";
import SignUpPage from "../pages/SignUpPage";
import NoAuthLayout from "../../layout/NoAuthLayout";
import AccountDetailsPage from "../pages/AccountDetails";
import AuthLayout from "../../layout/AuthLayout";
import TempAdminUserLayout from "../../layout/TempAdminUserLayout";
import {Card, Typography} from "@mui/material";
// layouts
//

// ----------------------------------------------------------------------

export default function PrettierAppRouter() {
    const routes = useRoutes([
        {
            path: '/',
            element: <NoAuthLayout/>,
            children: [
                {element: <Navigate to="/login"/>, index: true},
                {path: 'login', element: <LoginPage/>},
                {path: 'home', element: <HomePage/>},
                {path: 'about', element: <AboutMePage/>},
                {path: 'signUp', element: <SignUpPage/>},

            ],
        },
        {
            element: <AuthLayout/>,
            children: [
                {path: 'account', element: <AccountDetailsPage/>},
                {path: 'user-detials', element: <>
                    <Card>
                        <Typography variant="h2">Pudsahh</Typography>
                    </Card>
                    </>},
            ],
        },
        {
            path: 'users2',
            element: <>
                USERS 2
            </>,
        },

        {
            element: <TempAdminUserLayout/>,
            children: [
                {path: 'account', element: <AccountDetailsPage/>},
                {
                    path: 'users', element: <>
                        Tablica
                    </>
                },
            ],
        },
        {
            element: <SimpleLayout/>,
            children: [
                {path: '404', element: <Page404/>},
                {path: '*', element: <Navigate to="/404"/>},
            ],
        },
        {
            path: '*',
            element: <Navigate to="/404" replace/>,
        },

    ]);

    return routes;
}
