import {Container} from "react-bootstrap";

export default function AboutMePage({user,teacher}) {

    return (

        <Container>
            about me
            <p>Name: {user.name}</p>
            <p>Age: {user.age}</p>
            <p>teacher: {teacher.name} {teacher.age}</p>

        </Container>

    )

}
