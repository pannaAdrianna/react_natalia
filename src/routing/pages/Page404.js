import {Container} from "react-bootstrap";

export default function Page404() {
    return (
        <>
            <Container>
                    Sorry, page not found!
            </Container>
        </>
    )}
