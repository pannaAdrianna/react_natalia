import {Button, Card, TextField, Typography} from "@mui/material";
import BMI from './model/BMI.js';

function calculateBMI() {
    let height = document.getElementById('height').value;
    let weight = document.getElementById('weight').value;
    let bmi = new BMI(height, weight);
    document.getElementById('result').textContent = bmi.showResult();
}

export const CalculatorBMI = () => {
    return (
        <Card>
            <Typography variant="h1">Weight:</Typography>
            <TextField id="weight" label="Weight" variant="outlined" />
            <Typography variant="h1">Height:</Typography>
            <TextField id="height" label="Height" variant="outlined" />
            <Typography id="result" variant="h1">BMI: </Typography>
            <Button onClick={calculateBMI}>Calculate</Button>
        </Card>
    )
}
