import {Outlet} from 'react-router-dom';
// @mui
import {styled} from '@mui/material/styles';
import Container from "react-bootstrap/Container";
import AppNavbarUser from "../routing/navbar/AppNavbarUser";
import AppNavbarAdmin from "../routing/navbar/AppNavbarAdmin";
// components


// ----------------------------------------------------------------------

const StyledHeader = styled('header')(({theme}) => ({
    top: 0,
    left: 0,
    lineHeight: 0,
    width: '100%',
    position: 'absolute',
    padding: theme.spacing(3, 3, 0),
    [theme.breakpoints.up('sm')]: {
        padding: theme.spacing(5, 5, 0),
    },
}));

// ----------------------------------------------------------------------

export default function TempAdminUserLayout() {
    return (
        <>
            <AppNavbarAdmin/>
            <Container>
                <Outlet/>
            </Container>
        </>
    )
        ;
}
