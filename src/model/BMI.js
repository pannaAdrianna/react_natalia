export default class BMI {
    constructor(height, weight) {
        this.height = height;
        this.weight = weight;
    }
    calculateBMI() {
        let height = this.height / 100;
        let bmi = this.weight / (height * height);
        return this.roundMath(bmi);
    }
    roundMath(bmi) {
        return Math.round(bmi);
    }
    showResult() {
        return "BMI: " + this.calculateBMI();
    }
}