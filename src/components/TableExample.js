import React, {useState} from 'react';

// material
import {Container, Table, TableBody, TableCell, TableContainer, TableRow, Typography,} from '@mui/material';
import TableHeaders from "./TableHeaderList";
import Label from "./Label";
// components


const TABLE_HEAD = [
    {id: 'lp', label: 'lp', alignRight: false},
    {id: 'id', label: 'id', alignRight: false},
    {id: 'name', label: 'Name', alignRight: false},
    {id: 'name', label: 'Red name if Isacco', alignRight: false},
    {id: 'bmi', label: 'BMI', alignRight: false},
    {id: ''},
];


export const TableExample = ({dataToTable}) => {


    const [tableData, setTableData] = useState(dataToTable);
    // można też ominąć tą linijkę i w


    const [order, setOrder] = useState('asc');
    const [selected, setSelected] = useState([]);
    const [orderBy, setOrderBy] = useState('name');

    return (
        <Container>
            <Typography variant='h3' gutterBottom>
                BMI List
            </Typography>
            <TableContainer sx={{minWidth: 800}}>
                <Table size={'small'}>
                    <TableHeaders
                        order={order}
                        orderBy={orderBy}
                        headLabel={TABLE_HEAD}
                        rowCount={tableData.length}
                        numSelected={selected.length}
                        isRent={false}


                    />
                    <TableBody>

                        {tableData.map((row, index) => (
                            <TableRow key={row.id}>
                                <TableCell align='left'>{index+1}</TableCell>
                                <TableCell align='left'>id: {row.id}</TableCell>
                              <TableCell align="left">
                                    <Label variant="ghost"
                                           color={(row.name.includes('o') && 'error') || 'success'}>
                                        {row.name}
                                    </Label>
                                </TableCell>

                                <TableCell align="left">
                                    <Label variant="ghost"
                                           color={(row.name==="Isacco"&& 'error') || 'success'}>
                                        {row.name}
                                    </Label>
                                </TableCell>
                                <TableCell align='left'>{row.bmi}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>

                </Table>
            </TableContainer>


        </Container>

    )
        ;
}
